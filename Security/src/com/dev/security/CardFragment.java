package com.dev.security;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fima.cardsui.views.CardUI;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13. 10. 12
 * Time: 오전 6:40
 * To change this template use File | Settings | File Templates.
 */
public class CardFragment extends Fragment {

    private CardUI mCardView;

    private String title, desc;
    private int resId, resDesc = -1;

    public CardFragment(int resId, String title) {
        this.title = title;
        this.resId = resId;
        this.desc = "";
    }

    public CardFragment(int resId, String title, String desc) {
        this.title = title;
        this.resId = resId;
        this.desc = desc;
    }

    public CardFragment(int resId, String title, int desc) {
        this.title = title;
        this.resId = resId;
        this.resDesc = desc;
    }

    private final static int[] colors = {R.color.red, R.color.orange, R.color.green, R.color.blue, R.color.purple};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main2, container, false);

        // init CardView
        mCardView = (CardUI) v.findViewById(R.id.cardsview);

        MyImageCard card = new MyImageCard(title, resId);
        card.setCardDescription(resDesc != -1 ? getString(resDesc) : desc);
        mCardView.addCard(card);

        // Draw
        mCardView.refresh();

        Random r = new Random(System.currentTimeMillis());
        int c = r.nextInt(5);

        v.setBackgroundColor(Utils.diversifyColor(getResources().getColor(colors[c]), 0, -0.5f, 0.1f));
        return v;
    }
}
