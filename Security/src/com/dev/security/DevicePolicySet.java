package com.dev.security;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;
import org.jetbrains.annotations.NotNull;

public class DevicePolicySet extends DeviceAdminReceiver {

    static SharedPreferences getPreferences(@NotNull Context context) {
        return context.getSharedPreferences(DeviceAdminReceiver.class.getName(), 0);
    }

    ;

    void showToast(Context context, CharSequence msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        // if(기기권한이 설정)
        showToast(context, "기기 관리가 설정");
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        // if(기기 권한이 해제)
        showToast(context, "기기 관리자 해제");
    }

    @NotNull
    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "기기 관리자 권한을 해제하시면 더이상 저희가 당신의 핸드폰을 관리 할 수 없습니다!\n그래도 해제하시겠습니까?";
    }
}