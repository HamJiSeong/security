package com.dev.security;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13. 10. 11
 * Time: 오전 3:54
 * To change this template use File | Settings | File Templates.
 */
public class LocationHelper {

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    private Context ct;
    private LocationManager manager;
    private LocationListener gpsListener, netListener;
    private ArrayList<Location> gpsQueue, netQueue;

    public LocationHelper(Context context) {
        this.ct = context;
        setUp();
    }

    @Override
    protected void finalize() throws Throwable {
        setOff();
        super.finalize();
    }

    private void setUp() {
        manager = (LocationManager) ct.getSystemService(Context.LOCATION_SERVICE);
        gpsQueue = new ArrayList<Location>();
        netQueue = new ArrayList<Location>();

        // Define a listener that responds to location updates
        gpsListener = new LocationListener() {
            public void onLocationChanged(@NotNull Location location) {
                // Called when a new location is found by the network location provider.
                makeUseOfNewGpsLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        netListener = new LocationListener() {
            public void onLocationChanged(@NotNull Location location) {
                // Called when a new location is found by the network location provider.
                makeUseOfNewNetLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpsListener);
        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, netListener);
    }

    private void setOff() {
        manager.removeUpdates(gpsListener);
        manager.removeUpdates(netListener);
    }

    private void makeUseOfNewGpsLocation(@NotNull Location location) {
        if (gpsQueue.isEmpty()) gpsQueue.add(location);
        else if (isBetterLocation(location, gpsQueue.get(gpsQueue.size() - 1))) gpsQueue.add(location);
    }

    private void makeUseOfNewNetLocation(@NotNull Location location) {
        if (netQueue.isEmpty()) netQueue.add(location);
        else if (isBetterLocation(location, netQueue.get(netQueue.size() - 1))) netQueue.add(location);
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     * @param location            The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(@NotNull Location location, @Nullable Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(@Nullable String provider1, @Nullable String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Nullable
    public Location getLastLocation() {
        if (netQueue.isEmpty()) {
            if (gpsQueue.isEmpty())
                return null;
            else
                return gpsQueue.get(gpsQueue.size() - 1);
        } else {
            if (gpsQueue.isEmpty())
                return netQueue.get(netQueue.size() - 1);
            else
                return isBetterLocation(gpsQueue.get(gpsQueue.size() - 1), netQueue.get(netQueue.size() - 1)) ? gpsQueue.get(gpsQueue.size() - 1) : netQueue.get(gpsQueue.size() - 1);
        }
    }
}
