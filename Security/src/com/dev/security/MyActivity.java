package com.dev.security;


import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import com.fima.cardsui.views.CardUI;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * @author OMM
 * @since 2013/10/11
 */
public class MyActivity extends ActionBarActivity {


    /**
     * 기기관리자 관련 함수 호출에 사용
     */
    public DevicePolicyManager devicePolicyManager;
    /**
     * 'Device Policy Set Receiver'를 인증하는데 사용
     *
     * @see DevicePolicySet
     */
    public ComponentName adminComponent;
    /**
     * 카드 형태의 UI 구현에 활용
     */
    private CardUI mCardView;

    private ViewPager pager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                i += 1;
                if (i == 1) {
                    return new CardFragment(R.drawable.dev3, "DEV", R.string.club_intro);
                }
                if (i == 2) {
                    return new CardFragment(R.drawable.dev3, "동아리 소개", R.string.club_info);
                }
                if (i == 3) {
                    return new CardFragment(R.drawable.dev3, "프로그램 소개");
                }
                if (i >= 4 && i <= 8) {
                    return new CardFragment(R.drawable.dev3, "프로그램 소개", R.string.intro101 + i - 4);
                }
                if (i == 9) {
                    return new CardFragment(R.drawable.dev3, "필요한 도구나 재료");
                }
                if (i >= 10 && i <= 10) {
                    return new CardFragment(R.drawable.dev3, "필요한 도구나 재료", R.string.intro201 + i - 10);
                }
                if (i == 11) {
                    final LocationHelper helper = new LocationHelper(getApplicationContext());
                    final DevicePolicyManager mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

                    // 초기화
                    adminComponent = new ComponentName(MyActivity.this, DevicePolicySet.class);
                    devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

                    if (!devicePolicyManager.isAdminActive(adminComponent)) {   // 기기 관리자로 등록할 것인지 확인
                        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminComponent);
                        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Explanations");
                        startActivityForResult(intent, 1);
                    }   // 강제 등록방법은 현재 존재하지 않음 ㅇㅇ
                    // 레퍼런스까지 다 뒤져봤으나 존재하지 않았음.
                    return new CardFragment(R.drawable.dev3, "활동 방법");
                }
                if (i >= 12 && i <= 16) {
                    return new CardFragment(R.drawable.dev3, "활동 방법", R.string.intro301 + i - 12);
                }
                if (i == 17) {
                    return new CardFragment(R.drawable.dev3, "해킹의 원리");
                }
                if (i >= 18 && i <= 20) {
                    return new CardFragment(R.drawable.dev3, "해킹의 원리", R.string.intro401 + i - 18);
                }
                return null;
            }

            @Override
            public int getCount() {
                return 20;
            }
        });

        //Bind the title indicator to the adapter
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        pager.setCurrentItem(0);
        indicator.setFillColor(getResources().getColor(R.color.more_green));
    }
}
