package com.dev.security;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fima.cardsui.objects.Card;
import org.jetbrains.annotations.NotNull;

public class MyCard extends Card {

    private int t_color = 0;
    private int c_color = 0;

    static public enum CardColor {
        RED, GREEN, BLUE, ORANGE, PURPLE
    }

    ;

    public MyCard(String title) {
        super(title);
    }

    @Override
    public View getCardContent(@NotNull Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_ex, null);
        ((TextView) view.findViewById(R.id.title)).setText(title);
        if (desc != null) ((TextView) view.findViewById(R.id.description)).setText(desc);
        if (t_color != 0) {
            ((LinearLayout) view.findViewById(R.id.card_title_space)).setBackgroundColor(context.getResources().getColor(t_color));
            ((LinearLayout) view.findViewById(R.id.card_whole_space)).setBackgroundColor(context.getResources().getColor(t_color));
            ((TextView) view.findViewById(R.id.title)).setTextColor(Color.WHITE);
            ((TextView) view.findViewById(R.id.description)).setTextColor(Color.WHITE);
        }
        if (c_color != 0)
            ((LinearLayout) view.findViewById(R.id.card_content_space)).setBackgroundColor(context.getResources().getColor(c_color));
        return view;
    }

    public void setCardColor(CardColor color) {
        applyColor(color);
    }

    public void setCardDescription(String desc) {
        this.desc = desc;
    }

    private void applyColor(CardColor color) {
        if (color == CardColor.RED) {
            t_color = R.color.more_red;
            c_color = R.color.red;
        } else if (color == CardColor.ORANGE) {
            t_color = R.color.more_orange;
            c_color = R.color.orange;
        } else if (color == CardColor.ORANGE) {
            t_color = R.color.more_orange;
            c_color = R.color.orange;
        } else if (color == CardColor.PURPLE) {
            t_color = R.color.more_purple;
            c_color = R.color.purple;
        } else if (color == CardColor.GREEN) {
            t_color = R.color.more_green;
            c_color = R.color.green;
        } else if (color == CardColor.BLUE) {
            t_color = R.color.more_blue;
            c_color = R.color.blue;
        }
    }
}
