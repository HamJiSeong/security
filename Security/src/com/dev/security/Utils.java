package com.dev.security;

import android.graphics.Color;
import android.telephony.SmsManager;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13. 10. 8
 * Time: 오후 1:57
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

    /**
     * This is a method for diversifying color by changing a single toned color into a hued, saturated, or brightened toned color.
     *
     * @param color 원본 색상
     * @param hue   색상
     * @param sat   채도
     * @param bri   명도
     * @return 변환된 색상
     */
    public static int diversifyColor(int color, float hue, float sat, float bri) {
        float[] hsv = new float[3];         // Array to store HSV values
        Color.colorToHSV(color, hsv);       // Get original HSV values of pixel
        hsv[0] += hue;                      // Add the shift to the HUE of HSV array
        hsv[0] %= 360;                      // Confines hue to values:[0,360]
        hsv[1] += sat;                      // Confines saturation to values:[0,1]
        hsv[2] += bri;                      // Confines brightness to values:[0,1]
        return Color.HSVToColor(Color.alpha(color), hsv);
    }

    public static void sendSMS(String sendTo, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(sendTo, null, msg, null, null);
    }

}
